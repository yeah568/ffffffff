var PIXI = require('./pixi.min.js');
require('./pixi.ninepatch.js');
var io = require('socket.io-client');

var renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, {backgroundColor : 0x0089b3});
document.body.appendChild(renderer.view);

var width = window.innerWidth;
var height = window.innerHeight;

// create the root of the scene graph
var stage = new PIXI.Container();

PIXI.loader
    .add("img/ui-pixel/9-Slice/Colored/green.png")
    .load(setup);

var buttons = [];
var text;

function Button() {
    this.sprite = new PIXI.Sprite(
      PIXI.loader.resources["img/ui-pixel/9-Slice/Colored/green.png"].texture
    );
    this.sprite.interactive = true;
    this.sprite.on('mousedown', buttonClick)
               .on('touchstart', buttonClick);
   // i am so ashamed
    this.sprite.buttonObject = this;
    this.counter = 0;
    this.text = new PIXI.Text(''+this.counter, {font:"28px Arial", fill:"red"});
}


function buttonClick(data) {
    data.target.buttonObject.counter++;
    data.target.buttonObject.text.text = ''+data.target.buttonObject.counter;
}

function setup() {
    var buttonContainer = new PIXI.Container();
    
    for (var i = 0; i < width/50 ; i++) {
        for (var j = 0; j < height/50; j++) {
            var button = new Button();
            button.sprite.x = i*50;
            button.sprite.y = j*50;
            button.text.anchor.x = 0.5;
            button.text.anchor.y = 0.5;
            button.text.x = button.sprite.width * 0.5;
            button.text.y = button.sprite.height * 0.5;
            button.sprite.addChild(button.text);
            buttons.push(button);
            buttonContainer.addChild(button.sprite);
        }
    }
    
    buttonContainer.x = 0.5 * width - 0.5*buttonContainer.width;
    buttonContainer.y = 0.5 * height - 0.5*buttonContainer.height;
    stage.addChild(buttonContainer);

    animate();
}

// start animating
function animate() {
    requestAnimationFrame(animate);
    
    
    
    // render the container
    renderer.render(stage);
}
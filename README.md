# ffffffff
a real-time party game for friends (or enemies)


## Pronounciation

Acceptable pronounciations of "ffffffff" include:

* "eff eff eff eff eff eff eff eff"
* "eight eff"
* "eff eight"
* "four billion two hundred ninety four million nine hundred sixty seven thousand two hundred ninety five"
* /fːːːːːːː/
* "white"
* "the letter f eight times"

Pull requests accepted for alternate pronounciations.
